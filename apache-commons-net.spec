Name:           apache-commons-net
Version:        3.11.1
Release:        1
Summary:        Java library for Internet protocol
License:        Apache-2.0
URL:            http://commons.apache.org/net/
Source0:        https://github.com/apache/commons-net/archive/refs/tags/rel/commons-net-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  maven-local mvn(junit:junit) mvn(org.apache.commons:commons-parent:pom:)
BuildRequires:  mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:  mvn(org.codehaus.mojo:build-helper-maven-plugin)
BuildRequires:  mvn(org.codehaus.mojo:exec-maven-plugin)
BuildRequires:  mvn(org.junit.jupiter:junit-jupiter-api)
BuildRequires:  mvn(org.junit.jupiter:junit-jupiter-engine)
BuildRequires:  mvn(org.junit.vintage:junit-vintage-engine)
BuildRequires:  mvn(org.jacoco:jacoco-maven-plugin)
BuildRequires:  java-17-openjdk-devel
Requires:       java-17-openjdk

%description
Apache Commons Net library contains a collection of network utilities and protocol implementations.
Supported protocols include: Echo, Finger, FTP, NNTP, NTP, POP3(S), SMTP(S), Telnet, Whois

%package       help
Summary:       Help document for apache-commons-net
Provides:      %{name}-javadoc = %{version}-%{release}
Obsoletes:     %{name}-javadoc < %{version}-%{release}

%description   help
Help document for apache-commons-net.

%prep
%autosetup -n commons-net-rel-commons-net-%{version} -p1

%pom_remove_dep org.apache.ftpserver:ftpserver-core
%pom_remove_plugin :maven-checkstyle-plugin

%pom_xpath_inject pom:build/pom:plugins "
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-surefire-plugin</artifactId>
    <version>2.22.1</version>
    <configuration>
        <skipTests>true</skipTests>
    </configuration>
</plugin>"

rm \
src/test/java/org/apache/commons/net/chargen/CharGenUDPClientTest.java \
src/test/java/org/apache/commons/net/daytime/DaytimeTCPClientTest.java \
src/test/java/org/apache/commons/net/daytime/DaytimeUDPClientTest.java \
src/test/java/org/apache/commons/net/discard/DiscardUDPClientTest.java \
src/test/java/org/apache/commons/net/echo/EchoUDPClientTest.java \
src/test/java/org/apache/commons/net/ftp/AbstractFtpsTest.java \
src/test/java/org/apache/commons/net/ftp/FTPClientTransferModeTest.java \
src/test/java/org/apache/commons/net/ftp/FTPSClientTest.java \
src/test/java/org/apache/commons/net/ftp/NoProtocolSslConfigurationProxy.java \
src/test/java/org/apache/commons/net/tftp/TFTPAckPacketTest.java \
src/test/java/org/apache/commons/net/tftp/TFTPDataPacketTest.java \
src/test/java/org/apache/commons/net/tftp/TFTPErrorPacketTest.java \
src/test/java/org/apache/commons/net/tftp/TFTPReadRequestPacketTest.java \
src/test/java/org/apache/commons/net/tftp/TFTPServerPathTest.java \
src/test/java/org/apache/commons/net/tftp/TFTPTest.java \
src/test/java/org/apache/commons/net/tftp/TFTPWriteRequestPacketTest.java \
src/test/java/org/apache/commons/net/time/TimeTCPClientTest.java \
src/test/java/org/apache/commons/net/time/TimeUDPClientTest.java \

%mvn_file  : commons-net apache-commons-net
%mvn_alias : org.apache.commons:commons-net

%build
export JAVA_HOME=%{_jvmdir}/java-17-openjdk
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
%mvn_build --xmvn-javadoc

%install
%mvn_install

%files -f .mfiles
%license LICENSE.txt NOTICE.txt

%files help -f .mfiles-javadoc
%doc README.md RELEASE-NOTES.txt

%changelog
* Tue Jan 14 2025 yaoxin <1024769339@qq.com> - 3.11.1-1
- Update to 3.11.1:
  * Allow longer data in pattern IMAPReply.
  * Precompile regular expression in UnixFTPEntryParser.preParse(List<String>).
  * Guard against polynomial regular expression used on uncontrolled data in VMSVersioningFTPEntryParser.
  * Guard against polynomial regular expression used on uncontrolled data in IMAPReply.TAGGED_RESPONSE.
  * Cannot connect to FTP server with HTTP proxy. Fixes https://issues.apache.org/jira/browse/NET-730.
  * Base 64 Encoding with URL and Filename Safe Alphabet should not chunk per RFC 4648.
- Please see log: https://commons.apache.org/proper/commons-net/changes-report.html

* Fri Oct 27 2023 yaoxin <yao_xin001@hoperun.com> - 3.10.0-1
- Upgrade to 3.10.0

* Mon Jun 05 2023 wangkai <13474090681@163.com> - 3.6-6
- Remove TestConnectTimeout for ftp

* Wed Dec 4 2019 shijian <shijian16@huawei.com> - 3.6-5
- Package init
